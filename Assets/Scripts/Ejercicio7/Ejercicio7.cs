﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ejercicio7 : MonoBehaviour {

    public TrashPeluches imagenContenedora;
   
    public GameObject[] peluches = new GameObject[12];
    private int panel0Catidad;
    private int panel1Catidad;
    private int panel2Catidad;
    public Text cantPinguinosText;
    public Text cantOsosText;
    public Text cantPeluchesText;
    private int cantidadPinguinos;
    private int cantidadOsos;
    private int cantidadPeluches;
 // vidas y gamover 
    private int siguiente ;

    public GameObject gameOver;
    public GameObject win;
    public GameObject vidaOB;

    public float vida;

    private GameObject GM1;
    private GameObject GM2;
    private GameObject GM3;
    // Use this for initialization

    int random;
  

    public Text textRandom;
    void Start () {

    this.vida = 3;
    GM1 = GameObject.Find("vida1");
    GM2 = GameObject.Find("vida2");
    GM3 = GameObject.Find("vida3");

    siguiente = 7;
    cantidadPinguinos =  generarRandom(1,6);
    cantidadOsos = generarRandom(1,6);
    cantidadPeluches = cantidadOsos+ cantidadPinguinos;
   
    //activarPanel2();
    //textRandom.text = random.ToString();


        cantPeluchesText.text = cantidadPeluches.ToString();
    }
    
    // Update is called once per frame
    void Update () {
    cantPinguinosText.text = cantidadPinguinos.ToString();
    cantOsosText.text = cantidadOsos.ToString();
    }
    

    public void resultado () {
        //Debug.Log("random " + panel2Catidad);
        Debug.Log(imagenContenedora.getContadorPinguino()+"canridad pinguinos");
Debug.Log(cantidadPinguinos);
Debug.Log(imagenContenedora.getContadorOso()+"canridad Osos");
Debug.Log(cantidadOsos);
    if (cantidadPinguinos == imagenContenedora.getContadorPinguino() && cantidadOsos== imagenContenedora.getContadorOso() ) {//
            Debug.Log("ganaste");
            StartCoroutine(correcto());
        }else {
            Debug.Log("perdiste");
            setVida ();
        }
    }

        public void setVida () {
        this.vida--;
        StartCoroutine(Explode());
        eliminarVidas();
        
    }

 private void eliminarVidas  () {
        if (this.vida == 2) {
            Destroy(GM3);
        } else if (this.vida == 1) {
            Destroy(GM2);
        } else  {
            Destroy(GM1);
            StartCoroutine(finalizar());
            
        }
    }

    IEnumerator Explode()
    {
       this.vidaOB.SetActive(true);
       yield return new WaitForSeconds(0.9f);
       this.vidaOB.SetActive(false);

            
    }

    IEnumerator finalizar()
    {

       yield return new WaitForSeconds(0.9f);
       this.gameOver.SetActive(true);

            
    }

     IEnumerator correcto()
    {
            this.win.SetActive(true);
       yield return new WaitForSeconds(2.0f);
       SceneManager.LoadScene(siguiente);

            
    }


        private int  generarRandom (int valorMin, int valorMax) {
        return Random.Range (valorMin, valorMax);
    }
}