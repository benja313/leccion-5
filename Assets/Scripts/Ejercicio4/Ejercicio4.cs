﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ejercicio4 : MonoBehaviour {

	
	//private ButtonScript [] buttonScripts = new ButtonScript [9];

 // vidas y gamover
	public GameObject [] pelotasBasquet = new GameObject[6];
    public GameObject [] pelotasFutbol = new GameObject[6];
    public GameObject [] pelotasJuntasArry = new GameObject[8];
	public GameObject gameOver;
    public GameObject win;
    public GameObject vidaOB;

    public float vida;

    public GameObject vida1;
    public GameObject vida2;
    public GameObject vida3;
    private int randomFutbol;
    private int randomBasquetbol;
    private int pelotasJuntas;
    private int maxValor;
    private int cantidadVacio1;
    private int unidad;
    public Text texto;
	// Use this for initialization
	void Start () {
    this.vida = 3;
    randomFutbol = Random.Range (1, 4);

    randomBasquetbol = Random.Range(1, 4);
    pelotasJuntas = randomFutbol + randomBasquetbol;
    activarPelotasFutbol();
    activarPelotasJuntas();
    activarPelotasBasquetbol();
	}
	
	// Update is called once per frame
	void Update () {
		texto.text = unidad.ToString();
	}
    private void activarPelotasFutbol(){
        Debug.Log(randomFutbol+"pelotas futbol");
        for(int i = 0; i<randomFutbol;i++){
            pelotasFutbol[i].SetActive(true);
        }
    }
    private void activarPelotasBasquetbol(){
        Debug.Log(randomBasquetbol+"pelotas basque");
        for(int i = 0; i<randomBasquetbol;i++){
            pelotasBasquet[i].SetActive(true);
        }
    }
    private void activarPelotasJuntas(){
        Debug.Log(pelotasJuntas+"pelotas juntas");
        for(int i = 0; i<randomFutbol;i++){
            pelotasJuntasArry[i].SetActive(true);
        }
        for(int i = 4; i<4+randomBasquetbol;i++){
            pelotasJuntasArry[i].SetActive(true);
        }

    }

    public void incrementarUnidad(){
        unidad++;

    }
    public void disminuirUnidad(){
        unidad--;
    }
    public void comprobar(){
        if(unidad==pelotasJuntas){
            StartCoroutine(Ganar());
        }else{
            setVida();
        }
    }

	public void setVida () {
        this.vida--;
        StartCoroutine(Explode());
        eliminarVidas();
        
    }

    private void eliminarVidas  () {
        if (this.vida == 2) {
            Destroy(vida3);
        } else if (this.vida == 1) {
            Destroy(vida2);
        } else  {
            Destroy(vida1);
            this.gameOver.SetActive(true);
            SceneManager.LoadScene(0);
            
        }
    }

    IEnumerator Explode()
    {
       this.vidaOB.SetActive(true);
       yield return new WaitForSeconds(0.9f);
       this.vidaOB.SetActive(false);
    }
     IEnumerator Ganar()
    {
       this.win.SetActive(true);
       yield return new WaitForSeconds(2.0f);
       this.win.SetActive(false);
       SceneManager.LoadScene(4);
    }


}
