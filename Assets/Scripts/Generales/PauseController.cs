﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseController : MonoBehaviour {


	bool controlMenu = false;
	Canvas canvas;
	// Use this for initialization
	void Start () {
		canvas = GetComponent<Canvas>();
		canvas.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		InputHandler();
	}

	void InputHandler()
	{
		if(Input.GetKeyDown("escape"))
		{
			Debug.Log("exc");
			controlMenu = !controlMenu;
			canvas.enabled = controlMenu;
			Time.timeScale = (controlMenu) ? 0 : 1f;
		}
	}

		public void despausar () {
    	controlMenu = !controlMenu;
			canvas.enabled = controlMenu;
			Time.timeScale = (controlMenu) ? 0 : 1f;
    }
}
