﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InputBotonesScripts : MonoBehaviour {

	public Text valor_t;
	int valor = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  public void incrementarValor(){
  	this.valor++;
		valor_t.text = valor.ToString();
  }

	public void disminuirValor(){
  	this.valor--;	
  	valor_t.text = valor.ToString();
  }

  public int getValor () {
  	return this.valor;
  }
}
