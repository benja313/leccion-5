﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerColeccionesScript : MonoBehaviour {

		public Text timer_t;
	public Text coins_t;

	//public float time;

	// Use this for initialization
	void Start () {
			
		coins_t.text = ProjectVars.Instance.coin.ToString();
		Debug.Log(pasaraMinutos());
		timer_t.text = pasaraMinutos();
	}
	
	// Update is called once per frame
	void Update () {
		ProjectVars.Instance.time -= Time.deltaTime;
		if((int) ProjectVars.Instance.time <= 0)
		{
			timer_t.text = "0";
			SceneManager.LoadScene(0);
		}
		else if((int)ProjectVars.Instance.time > 0)
			timer_t.text = pasaraMinutos();

	}

	    public void SetCoins(int amount)
	{

			coins_t.text = amount.ToString();
	}


	public string pasaraMinutos() {

		int minutos = 0; 
		int segundos = 0;
		minutos = ((int)ProjectVars.Instance.time)/60;
		segundos = (int)((ProjectVars.Instance.time)%60);


		string tiempo = minutos + " : " + segundos;
		return tiempo;
	}
}
