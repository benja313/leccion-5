﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ejercicio5 : MonoBehaviour {

    public Trassh imagenContenedora;
    public GameObject[] panel0 = new GameObject[6];
    public GameObject[] panel1 = new GameObject[6];
    public GameObject[] cerezas = new GameObject[12];
    private int panel0Catidad;
    private int panel1Catidad;
    private int panel2Catidad;
 // vidas y gamover 
    private int siguiente ;

    public GameObject gameOver;
    public GameObject win;
    public GameObject vidaOB;

    public float vida;

    private GameObject GM1;
    private GameObject GM2;
    private GameObject GM3;
    // Use this for initialization

    int random;
  

    public Text textRandom;
    void Start () {

    this.vida = 3;
    GM1 = GameObject.Find("vida1");
    GM2 = GameObject.Find("vida2");
    GM3 = GameObject.Find("vida3");

    siguiente = 6;
    panel0Catidad =  generarRandom(1,6);
    panel1Catidad = generarRandom(1,6);
    panel2Catidad = panel1Catidad+ panel0Catidad;
    activarPanel0();
    activarPanel1();
    //activarPanel2();
    //textRandom.text = random.ToString();


        
    }
    
    // Update is called once per frame
    void Update () {
        
    }
    private void activarPanel0(){
        for(int i= 0;i<panel0Catidad;i++){
            panel0[i].SetActive(true);
        }
    }
    private void activarPanel1(){
        for(int i= 0;i<panel1Catidad;i++){
            panel1[i].SetActive(true);
        }
    }
    private void activarPanel2(){
        for(int i= 0;i<12;i++){
            cerezas[i].SetActive(true);
        }
    }



    public void resultado () {
        Debug.Log("random " + panel2Catidad);
        Debug.Log(imagenContenedora.getContador());
    if (panel2Catidad == imagenContenedora.getContador() ) {//
            Debug.Log("ganaste");
            StartCoroutine(correcto());
        }else {
            Debug.Log("perdiste");
            setVida ();
        }
    }

        public void setVida () {
        this.vida--;
        StartCoroutine(Explode());
        eliminarVidas();
        
    }

 private void eliminarVidas  () {
        if (this.vida == 2) {
            Destroy(GM3);
        } else if (this.vida == 1) {
            Destroy(GM2);
        } else  {
            Destroy(GM1);
            StartCoroutine(finalizar());
            
        }
    }

    IEnumerator Explode()
    {
       this.vidaOB.SetActive(true);
       yield return new WaitForSeconds(0.9f);
       this.vidaOB.SetActive(false);

            
    }

    IEnumerator finalizar()
    {

       yield return new WaitForSeconds(0.9f);
       this.gameOver.SetActive(true);

            
    }

     IEnumerator correcto()
    {
            this.win.SetActive(true);
       yield return new WaitForSeconds(2.0f);
       SceneManager.LoadScene(siguiente);

            
    }


        private int  generarRandom (int valorMin, int valorMax) {
        return Random.Range (valorMin, valorMax);
    }
}