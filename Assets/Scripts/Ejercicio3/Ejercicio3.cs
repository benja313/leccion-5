﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ejercicio3 : MonoBehaviour {

    
    //private ButtonScript [] buttonScripts = new ButtonScript [9];

 // vidas y gamover
    public GameObject gameOver;
    public GameObject win;
    public GameObject vidaOB;

    public float vida;

    public GameObject vida1;
    public GameObject vida2;
    public GameObject vida3;
    
    private int unidad;
    public Text texto;
    // Use this for initialization
    void Start () {

    this.vida = 3;

    }
    
    // Update is called once per frame
    void Update () {
        texto.text = unidad.ToString();
    }


public void ComprobarCorrecto(){
        
        if(unidad==8){
            StartCoroutine(Ganar());
        }else{
            setVida();
        }
    }

        public void setVida () {
        this.vida--;
        StartCoroutine(Explode());
        eliminarVidas();
        
    }
    
    public void incremetarUnidad(){
        unidad++;
    }
    public void disminuirUnidad(){
        unidad--;
    }

    private void eliminarVidas  () {
        if (this.vida == 2) {
            Destroy(vida3);
        } else if (this.vida == 1) {
            Destroy(vida2);
        } else  {
            Destroy(vida1);
            this.gameOver.SetActive(true);
            SceneManager.LoadScene(0);
            
        }
    }

    IEnumerator Explode()
    {
       this.vidaOB.SetActive(true);
       yield return new WaitForSeconds(0.9f);
       this.vidaOB.SetActive(false);
    }
     IEnumerator Ganar()
    {
       this.win.SetActive(true);
       yield return new WaitForSeconds(2.0f);
       this.win.SetActive(false);
       SceneManager.LoadScene(3);
    }


}
