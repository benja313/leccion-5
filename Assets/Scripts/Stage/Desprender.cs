﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desprender : MonoBehaviour {

	// Use this for initialization
	Rigidbody2D m_Rigidbody;
  Vector3 m_ZAxis;
  private bool gravedad;
	void Start () {
		gravedad = false;
		 m_Rigidbody = GetComponent<Rigidbody2D>();
       
        m_Rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
        m_ZAxis = new Vector3(0, 0, 0);
        m_Rigidbody.velocity = m_ZAxis;
	}
	
	// Update is called once per frame
	void Update () {
		if(gravedad){
			m_Rigidbody.constraints = RigidbodyConstraints2D.FreezePositionX;
			m_Rigidbody.velocity = new Vector3(0, -5, 9);
			m_Rigidbody.gravityScale = 9;
		}
	}
	public void setGravedad (){
		this.gravedad = true;
		Debug.Log(gravedad);
	}
	public void OnTrigger2D(Collision2D col)
	{
		Debug.Log("eLIMINAR BLOQUE!!");	
	}

	public void OnCollisionEnter2D(Collision2D col)
	{
		//Debug.Log("COLISIONE!!");
		if(col.gameObject.tag == "Player")
		{
			
			StartCoroutine(desprenderTime());
		}else{
			Debug.Log("eLIMINAR BLOQUE!!");	
		}
		if(col.gameObject.name == "Water")
		{
			Debug.Log("eLIMINAR BLOQUE!!");
			Destroy(this);
		}
	}
	IEnumerator desprenderTime()
    {
       
       yield return new WaitForSeconds(0.3f);
       setGravedad();
    }
}
