﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BotonGuitarraScript : MonoBehaviour {


	public GameObject guitarraNegra;
	public GameObject guitarraColor;


	private bool varible ;
	// Use this for initialization
	void Start () {

		varible = true;

	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void cambiarColorBoton () {

		if (varible) {

			guitarraNegra.SetActive(false);
			guitarraColor.SetActive(true);
		  varible = false;

		} else if (!varible) {

			guitarraNegra.SetActive(true);
			guitarraColor.SetActive(false);
		  varible = true;
		}
 
	}

	public bool getvariable () {
		return varible;
	}
}
