﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour {

	private Button boton;
	private ColorBlock theColor;

	private bool varibleColor = true;
	// Use this for initialization
	void Start () {
		boton = GetComponent<Button>();
		theColor = GetComponent<Button>().colors;

	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void cambiarColorBoton () {

		if (varibleColor) {
			theColor.highlightedColor = Color.blue;
	    theColor.normalColor = Color.blue;
	    theColor.pressedColor = Color.blue;
		  boton.colors = theColor;
		  varibleColor = false;

		} else if (!varibleColor) {
			theColor.highlightedColor = Color.white;
	    theColor.normalColor = Color.white;
	    theColor.pressedColor = Color.white;
		  boton.colors = theColor;
		  varibleColor = true;
		}
 
	}

	public bool getvariableColor () {
		return varibleColor;
	}


}
