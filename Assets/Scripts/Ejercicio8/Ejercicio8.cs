﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ejercicio8 : MonoBehaviour {

    public TrashAviones imagenContenedora;
   
    public GameObject[] aviones = new GameObject[12];
    private int panel0Catidad;
    private int panel1Catidad;
    private int panel2Catidad;
    public Text cantRojosText;
    public Text cantAzulesText;
    public Text cantAvionesText;
    private int cantidadRojos;
    private int cantidadAzules;
    private int cantidadAviones;
 // vidas y gamover 
    private int siguiente ;

    public GameObject gameOver;
    public GameObject win;
    public GameObject vidaOB;

    public float vida;

    private GameObject GM1;
    private GameObject GM2;
    private GameObject GM3;
    // Use this for initialization

    int random;
  

    public Text textRandom;
    void Start () {

    this.vida = 3;
    GM1 = GameObject.Find("vida1");
    GM2 = GameObject.Find("vida2");
    GM3 = GameObject.Find("vida3");

    siguiente = 17;
    cantidadAzules =  generarRandom(1,6);
    cantidadRojos = generarRandom(1,6);
    cantidadAviones = cantidadRojos+ cantidadAzules;
   
    //activarPanel2();
    //textRandom.text = random.ToString();


        cantAvionesText.text = cantidadAviones.ToString();
    }
    
    // Update is called once per frame
    void Update () {
    cantRojosText.text = cantidadRojos.ToString();
    cantAzulesText.text = cantidadAzules.ToString();
    }
    

    public void resultado () {
        //Debug.Log("random " + panel2Catidad);
       
    if (cantidadRojos == imagenContenedora.getContadorRojo() && cantidadAzules== imagenContenedora.getContadorAzul() ) {//
            Debug.Log("ganaste");
            StartCoroutine(correcto());
        }else {
            Debug.Log("perdiste");
            setVida ();
        }
    }

        public void setVida () {
        this.vida--;
        StartCoroutine(Explode());
        eliminarVidas();
        
    }

 private void eliminarVidas  () {
        if (this.vida == 2) {
            Destroy(GM3);
        } else if (this.vida == 1) {
            Destroy(GM2);
        } else  {
            Destroy(GM1);
            StartCoroutine(finalizar());
            
        }
    }

    IEnumerator Explode()
    {
       this.vidaOB.SetActive(true);
       yield return new WaitForSeconds(0.9f);
       this.vidaOB.SetActive(false);

            
    }

    IEnumerator finalizar()
    {

       yield return new WaitForSeconds(0.9f);
       this.gameOver.SetActive(true);

            
    }

     IEnumerator correcto()
    {
            this.win.SetActive(true);
       yield return new WaitForSeconds(2.0f);
       SceneManager.LoadScene(siguiente);

            
    }


        private int  generarRandom (int valorMin, int valorMax) {
        return Random.Range (valorMin, valorMax);
    }
}